variable "application_name" {
  default = "atillay"
}
variable "machine_type" {
  default = "e2-micro"
}
variable "gcp_project" {
  default = "sfeir-school-terraform"
}
