resource "google_compute_instance" "default" {
  name = "${var.application_name}-${terraform.workspace}"
  machine_type = var.machine_type
  zone         = "europe-west1-b"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }
  network_interface {
    network = "default"

    access_config {
    }
  }
  metadata_startup_script = "echo 'hello world'"
}
